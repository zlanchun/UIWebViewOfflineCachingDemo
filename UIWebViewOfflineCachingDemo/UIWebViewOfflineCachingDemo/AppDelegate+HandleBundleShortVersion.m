//
//  AppDelegate+HandleBundleShortVersion.m
//  UIWebViewOfflineCachingDemo
//
//  Created by zhenglanchun on 2017/7/1.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import "AppDelegate+HandleBundleShortVersion.h"

@implementation AppDelegate (HandleBundleShortVersion)
- (void)handleNewVersionWithBlock:(void (^)(BOOL newVersion))block {
    NSString *key = @"CFBundleShortVersionString";
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *lastVersion = [defaults stringForKey:key];
    NSString *currentVersion = [NSBundle mainBundle].infoDictionary[key];
    if ([currentVersion isEqualToString:lastVersion]) {// 旧版本
        block(NO);
    } else { // 新版本
        [defaults setObject:currentVersion forKey:key];
        block(YES);
    }
}
@end
