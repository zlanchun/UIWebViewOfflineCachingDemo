//
//  ViewController.m
//  UIWebViewOfflineCachingDemo
//
//  Created by z on 2017/6/30.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import "ViewController.h"
#import "RNCachingURLProtocol.h"

@interface ViewController ()<UIWebViewDelegate>
@property (strong,nonatomic) UIWebView *webView;
@property (nonatomic) CFAbsoluteTime startTime;
@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    UIWebView *webView = [[UIWebView alloc] initWithFrame:CGRectMake(0, 20, [UIScreen mainScreen].bounds.size.width, [UIScreen mainScreen].bounds.size.height)];
    webView.autoresizingMask = UIViewAutoresizingFlexibleHeight | UIViewAutoresizingFlexibleWidth;
    webView.scrollView.bounces = NO;
    webView.delegate = self;
    self.startTime =CFAbsoluteTimeGetCurrent();
    NSString *url =  @"https://0.u.7518.cn/";
    [webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
    
    [self.view addSubview:webView];
}

- (void)webViewDidFinishLoad:(UIWebView *)webView {
    CFAbsoluteTime endTime = (CFAbsoluteTimeGetCurrent() - self.startTime);
    NSLog(@"endTime in %f ms", endTime *1000.0);
    [RNCachingCustomStatus shared].loadFromCache = NO;
}

- (void)webView:(UIWebView *)webView didFailLoadWithError:(NSError *)error {
    // 当 cache 被清除后，通过 RNCachingURLProtocol 加载URL会失败，需要在这里进行处理
    [RNCachingCustomStatus shared].loadFromCache = NO;
    NSString *url =  @"https://0.u.7518.cn/";
    [self.webView loadRequest:[NSURLRequest requestWithURL:[NSURL URLWithString:url]]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
}


@end
