//
//  AppDelegate+HandleBundleShortVersion.h
//  UIWebViewOfflineCachingDemo
//
//  Created by zhenglanchun on 2017/7/1.
//  Copyright © 2017年 Pace.Z. All rights reserved.
//

#import "AppDelegate.h"

@interface AppDelegate (HandleBundleShortVersion)
- (void)handleNewVersionWithBlock:(void (^)(BOOL newVersion))block;
@end
